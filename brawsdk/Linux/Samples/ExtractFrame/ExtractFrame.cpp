/* -LICENSE-START-
 ** Copyright (c) 2018 Blackmagic Design
 **
 ** Permission is hereby granted, free of charge, to any person or organization
 ** obtaining a copy of the software and accompanying documentation covered by
 ** this license (the "Software") to use, reproduce, display, distribute,
 ** execute, and transmit the Software, and to prepare derivative works of the
 ** Software, and to permit third-parties to whom the Software is furnished to
 ** do so, all subject to the following:
 **
 ** The copyright notices in the Software and this entire statement, including
 ** the above license grant, this restriction and the following disclaimer,
 ** must be included in all copies of the Software, in whole or in part, and
 ** all derivative works of the Software, unless such copies or derivative
 ** works are solely in the form of machine-executable object code generated by
 ** a source language processor.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 ** SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 ** FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 ** ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 ** DEALINGS IN THE SOFTWARE.
 ** -LICENSE-END-
 */

#include "BlackmagicRawAPI.h"

#include <stdio.h>
#include <iostream>

#ifdef DEBUG
	#include <cassert>
	#define VERIFY(condition) assert(SUCCEEDED(condition))
#else
	#define VERIFY(condition) condition
#endif

static const BlackmagicRawResourceFormat s_resourceFormat = blackmagicRawResourceFormatRGBAU8;
static const char* const s_outputFileName = "outputRGBU8.bmp";

void OutputImage(unsigned int width, unsigned int height, void* imageData)
{
	FILE* file = fopen(s_outputFileName, "wb");
	if (file == nullptr)
		return;

	typedef struct                       /**** BMP file header structure ****/
    {
		unsigned int   bfSize;           /* Size of file */
		unsigned short bfReserved1;      /* Reserved */
		unsigned short bfReserved2;      /* ... */
		unsigned int   bfOffBits;        /* Offset to bitmap data */
    } BITMAPFILEHEADER;

	typedef struct                       /**** BMP file info structure ****/
    {
		unsigned int   biSize;           /* Size of info header */
		int            biWidth;          /* Width of image */
		int            biHeight;         /* Height of image */
		unsigned short biPlanes;         /* Number of color planes */
		unsigned short biBitCount;       /* Number of bits per pixel */
		unsigned int   biCompression;    /* Type of compression to use */
		unsigned int   biSizeImage;      /* Size of image data */
		int            biXPelsPerMeter;  /* X pixels per meter */
		int            biYPelsPerMeter;  /* Y pixels per meter */
		unsigned int   biClrUsed;        /* Number of colors used */
		unsigned int   biClrImportant;   /* Number of important colors */
    } BITMAPINFOHEADER;

	BITMAPFILEHEADER bfh;
	BITMAPINFOHEADER bih;

	unsigned short bfType = 0x4d42;
	bfh.bfReserved1 = 0;
	bfh.bfReserved2 = 0;
	bfh.bfSize = 2 + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + width * height * 3;
	bfh.bfOffBits = 0x36;

	bih.biSize = sizeof(BITMAPINFOHEADER);
	bih.biWidth = width;
	bih.biHeight = -(long)height;
	bih.biPlanes = 1;
	bih.biBitCount = 24;
	bih.biCompression = 0;
	bih.biSizeImage = 0;
	bih.biXPelsPerMeter = 5000;
	bih.biYPelsPerMeter = 5000;
	bih.biClrUsed = 0;
	bih.biClrImportant = 0;

	fwrite(&bfType,1,sizeof(bfType),file);
	fwrite(&bfh, 1, sizeof(bfh), file);
	fwrite(&bih, 1, sizeof(bih), file);

	unsigned char* rgba = (unsigned char*)imageData;

	for (int y = (int)height-1; y >= 0; y--) /*Scanline loop backwards*/
    {
		for (int x = 0; x < (int)width; x++) /*Column loop forwards*/
		{
			unsigned char red = rgba[0];
			unsigned char green = rgba[1];
			unsigned char blue = rgba[2];
			rgba += 4; // Skip alpha

			fwrite(&blue, 1, 1, file);
			fwrite(&green, 1, 1, file);
			fwrite(&red, 1, 1, file);
		}
	}
	fclose(file);
}

class CameraCodecCallback : public IBlackmagicRawCallback
{
public:
	explicit CameraCodecCallback() = default;
	virtual ~CameraCodecCallback() = default;

	virtual void ReadComplete(IBlackmagicRawJob* readJob, HRESULT result, IBlackmagicRawFrame* frame)
	{
		IBlackmagicRawJob* decodeAndProcessJob = nullptr;

		if (result == S_OK)
			VERIFY(frame->SetResourceFormat(s_resourceFormat));

		if (result == S_OK)
			result = frame->CreateJobDecodeAndProcessFrame(nullptr, nullptr, &decodeAndProcessJob);

		if (result == S_OK)
			result = decodeAndProcessJob->Submit();

		if (result != S_OK)
		{
			if (decodeAndProcessJob)
				decodeAndProcessJob->Release();
		}

		readJob->Release();
	}

	virtual void ProcessComplete(IBlackmagicRawJob* job, HRESULT result, IBlackmagicRawProcessedImage* processedImage)
	{
		unsigned int width = 0;
		unsigned int height = 0;
		void* imageData = nullptr;

		if (result == S_OK)
			result = processedImage->GetWidth(&width);

		if (result == S_OK)
			result = processedImage->GetHeight(&height);

		if (result == S_OK)
			result = processedImage->GetResource(&imageData);

		if (result == S_OK)
			OutputImage(width, height, imageData);

		job->Release();
	}

	virtual void DecodeComplete(IBlackmagicRawJob*, HRESULT) {}
	virtual void TrimProgress(IBlackmagicRawJob*, float) {}
	virtual void TrimComplete(IBlackmagicRawJob*, HRESULT) {}
	virtual void SidecarMetadataParseWarning(IBlackmagicRawClip*, const char*, uint32_t, const char*) {}
	virtual void SidecarMetadataParseError(IBlackmagicRawClip*, const char*, uint32_t, const char*) {}
	virtual void PreparePipelineComplete(void*, HRESULT) {}

	virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID, LPVOID*)
	{
		return E_NOTIMPL;
	}

	virtual ULONG STDMETHODCALLTYPE AddRef(void)
	{
		return 0;
	}

	virtual ULONG STDMETHODCALLTYPE Release(void)
	{
		return 0;
	}
};

int main(int argc, const char* argv[])
{
	if (argc > 2)
	{
		std::cerr << "Usage: " << argv[0] << " clipName.braw" << std::endl;
		return 1;
	}

	const char* clipName = nullptr;
	bool clipNameProvided = argc == 2;
	if (clipNameProvided)
	{
		clipName = argv[1];
	}
	else
	{
		clipName = "../../../Media/sample.braw";
	}

	HRESULT result = S_OK;

	IBlackmagicRawFactory* factory = nullptr;
	IBlackmagicRaw* codec = nullptr;
	IBlackmagicRawClip* clip = nullptr;
	IBlackmagicRawJob* readJob = nullptr;

	CameraCodecCallback callback;
	long readTime = 0;

	do
	{
		factory = CreateBlackmagicRawFactoryInstanceFromPath("../../Libraries/");
		if (factory == nullptr)
		{
			std::cerr << "Failed to create IBlackmagicRawFactory!" << std::endl;
			break;
		}

		result = factory->CreateCodec(&codec);
		if (result != S_OK)
		{
			std::cerr << "Failed to create IBlackmagicRaw!" << std::endl;
			break;
		}

		result = codec->OpenClip(clipName, &clip);
		if (result != S_OK)
		{
			std::cerr << "Failed to open IBlackmagicRawClip!" << std::endl;
			break;
		}

		result = codec->SetCallback(&callback);
		if (result != S_OK)
		{
			std::cerr << "Failed to set IBlackmagicRawCallback!" << std::endl;
			break;
		}

		result = clip->CreateJobReadFrame(readTime, &readJob);
		if (result != S_OK)
		{
			std::cerr << "Failed to create IBlackmagicRawJob!" << std::endl;
			break;
		}

		result = readJob->Submit();
		if (result != S_OK)
		{
			// submit has failed, the ReadComplete callback won't be called, release the job here instead
			readJob->Release();
			std::cerr << "Failed to submit IBlackmagicRawJob!" << std::endl;
			break;
		}

		codec->FlushJobs();

	} while(0);

	if (clip != nullptr)
		clip->Release();

	if (codec != nullptr)
		codec->Release();

	if (factory != nullptr)
		factory->Release();

	return result;
}
