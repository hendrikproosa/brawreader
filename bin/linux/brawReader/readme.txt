Blackmagic RAW format reader plugin for Nuke v1.0
Based on BRAW SDK version 1.7
Created by Hendrik Proosa
Date: May 11, 2020 
---------------------------------------------

This is first experimental version for Linux
Compiled for Nuke 11.3, 12.0 and 12.1
Tested on Centos 7.6

HOW TO USE
--------------------------------------------
Copy the /hpTools folder to your Nuke plugin path ~/.nuke
Copy contents of init.py and menu.py to respective files in plugin path
Copy contents of /braw_libs folder to Nuke installation directory (where Nuke executable is located)
^ this step isn't very nice, but I had a hard time getting Nuke find the SDK libs otherwise

sudo yum install libcxx-3.8.0-3.el7.x86_64

KNOWN ISSUES
--------------------------------------------
In my system reader properly works only when Nuke is run from command line. Why, I have no idea.
When run through desktop link, reader produces black frame and crashes. Must investigate.

Knobs are not changed when .braw file is opened so they don't reflect the stored processing values. Will be fixed.

Some frame level metadata keys show their values as 0 or missing, not sure if it is some kind of bug...

Lower-resolution decoding is not implemented, and I removed the option from knobs.
I'm not sure it is possible to change the resolution of reader on the fly, after creation of op instance.

Sidecar handling is not implemented, if it is present braw decoder should use it, but mechanisms to load or store processing
settings to sidecar at will isn't there yet.
