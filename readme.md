# Blackmagic RAW Reader for Nuke
Created by Hendrik Proosa.

Current version is a port for Linux, based on Braw SDK v1.7

I used example code in Braw SDK as basis for code.

## Build

Use libraries and headers from BRAW SDK version 1.7

### Linux

Use the makefile in /src dir, set your paths as necessary first.

### Win

I use MSVC 2010 x64 compiler from Win7 dev kit for Nuke 10.5
Nuke SDK reference guide has detailed instructions for compilation and dependencies.


## Install


### Linux
Copy the /bin/linux/hpTools folder to Nuke plugin path.

Copy the contents of init.py and menu.py in /bin/linux to respective files in your Nuke plugin path

Copy the contents of /bin/linux/braw_libs folder to Nuke executable folder (where Nuke is installed)


### Win
Copy the brawReader.dll file to Nuke plugins dir.

## What it does

Allows reading .braw files, just use Read node :)
Two example videos, both are old (from first iteration of plugin based on sdk v1.0):

https://vimeo.com/290682476

https://vimeo.com/290925954


## Known bugs and other annoyances

 
## Changelog

### 12. may 2020
Updated to braw sdk version 1.7, only Linux verson currently

### 25. september 2018
Updated docs

### 20. september 2018
Knobs and stuff working

### 14. september 2018
First try
