

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Mon Sep 17 14:40:32 2018
 */
/* Compiler settings for c:\tmp\rawtest\blackmagicrawapi.idl:
    Oicf, W1, Zp8, env=Win64 (32b run), target_arch=AMD64 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, LIBID_BlackmagicRawAPI,0x7B7EFAC7,0x22B8,0x4EA1,0x8F,0xAB,0x75,0x83,0xB6,0x10,0x0B,0x10);


MIDL_DEFINE_GUID(IID, IID_IBlackmagicRaw,0x5A540A06,0x1B62,0x4224,0xAC,0xB0,0xA2,0x38,0x5C,0x6E,0xD6,0x49);


MIDL_DEFINE_GUID(IID, IID_IBlackmagicRawFactory,0x74FEBEDC,0x12D6,0x490D,0x9A,0x77,0x48,0xF1,0x9E,0x8F,0x60,0xCB);


MIDL_DEFINE_GUID(IID, IID_IBlackmagicRawConstants,0x7EFC98CD,0xE6AE,0x4155,0xB6,0x4F,0x06,0x80,0xD8,0xE2,0x68,0x5C);


MIDL_DEFINE_GUID(IID, IID_IBlackmagicRawConfiguration,0x46F11AFB,0x16F4,0x483E,0x93,0x33,0x69,0xD9,0x4D,0xC1,0x34,0x4B);


MIDL_DEFINE_GUID(IID, IID_IBlackmagicRawConfigurationEx,0xACE9078F,0xABA0,0x4B26,0xA9,0x54,0xED,0xA1,0x08,0xDA,0xDA,0x5A);


MIDL_DEFINE_GUID(IID, IID_IBlackmagicRawResourceManager,0xDC09804F,0x7005,0x4AA7,0xAD,0xC9,0x12,0xE0,0xFE,0x33,0x89,0x6C);


MIDL_DEFINE_GUID(IID, IID_IBlackmagicRawMetadataIterator,0xF85AE78D,0x5DC2,0x40BC,0x8C,0x1D,0xD0,0xD8,0x05,0x52,0x3A,0xDA);


MIDL_DEFINE_GUID(IID, IID_IBlackmagicRawClipProcessingAttributes,0xD0931FE5,0x16DA,0x4BAC,0xBC,0x31,0xFE,0xC2,0x49,0x9B,0x36,0x8E);


MIDL_DEFINE_GUID(IID, IID_IBlackmagicRawFrameProcessingAttributes,0x9E6F4E88,0x63B5,0x4676,0xAD,0x1D,0x22,0xA5,0xDE,0xEB,0x13,0x7A);


MIDL_DEFINE_GUID(IID, IID_IBlackmagicRawProcessedImage,0xD87A0F72,0xA883,0x42BB,0x84,0x88,0x00,0x89,0x41,0x1C,0x50,0x35);


MIDL_DEFINE_GUID(IID, IID_IBlackmagicRawJob,0x34C05ACF,0x7118,0x45EA,0x8B,0x71,0x88,0x7E,0x05,0x15,0x39,0x5D);


MIDL_DEFINE_GUID(IID, IID_IBlackmagicRawCallback,0x762C4B66,0x2BDA,0x468B,0xBB,0x7B,0x2C,0x92,0x2A,0x63,0x7A,0x4A);


MIDL_DEFINE_GUID(IID, IID_IBlackmagicRawClipAudio,0x76D4ACED,0xE0D6,0x45BB,0xB5,0x47,0x56,0xB7,0x43,0x5B,0x2A,0x1D);


MIDL_DEFINE_GUID(IID, IID_IBlackmagicRawFrame,0x4DC7CA84,0xED8E,0x4E2E,0xA0,0x96,0x1C,0x62,0x07,0x9A,0x0F,0xB3);


MIDL_DEFINE_GUID(IID, IID_IBlackmagicRawFrameEx,0xF8C6C374,0xD7FB,0x4BD3,0xAD,0x0B,0xC5,0x33,0x46,0x4F,0xF4,0x50);


MIDL_DEFINE_GUID(IID, IID_IBlackmagicRawManualDecoderFlow1,0xDBA641BC,0x7B06,0x4251,0xB5,0x1C,0xFD,0x5E,0x2E,0x43,0x84,0x02);


MIDL_DEFINE_GUID(IID, IID_IBlackmagicRawManualDecoderFlow2,0xC0095147,0x4172,0x47BD,0xAD,0xF4,0x4F,0x33,0x7F,0xFB,0x12,0x5A);


MIDL_DEFINE_GUID(IID, IID_IBlackmagicRawClip,0x08834FDD,0x48D4,0x4DA2,0x8F,0x70,0x8A,0xC0,0x9A,0xA8,0x2F,0x53);


MIDL_DEFINE_GUID(IID, IID_IBlackmagicRawClipEx,0xD260C7D0,0x93BD,0x4D68,0xB6,0x00,0x93,0xB4,0xCA,0xB7,0xF8,0x70);


MIDL_DEFINE_GUID(CLSID, CLSID_CBlackmagicRawFactory,0xD630D04C,0x1434,0x4BF8,0x88,0x01,0xE2,0xAC,0x1F,0x56,0xC6,0xBA);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



