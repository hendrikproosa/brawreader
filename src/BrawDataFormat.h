// Class that holds stuff related to processing format
// This is used for storing and setting processing values in decoder
#include "BlackmagicRawAPI.h"
#include "DDImage/Knob.h"

class BrawDataFormat
{
public:
    BrawDataFormat();

    unsigned int width;
    unsigned int height;
    unsigned long long numFrames;
    unsigned long long inPoint;
    unsigned long long outPoint;
    float frameRate;
    const char* tcin;

    Variant decodeQuality;
    Variant colorScienceVersion;
    Variant colorSpaceGamut;
    Variant colorSpaceGamma;
    Variant iso;
    Variant exposure;
    Variant colorTemp;
    Variant tint;
    Variant saturation;
    Variant contrast;
    Variant midpoint;
    Variant highlights;
    Variant shadows;
    Variant setVideoBlackLevel;
    Variant blackLevel;
    Variant whiteLevel;
    Variant highlightRecovery;
    Variant analogGain;
    Variant embeddedPost3DLUTMode;
    Variant embeddedPost3DLUTName;
    Variant embeddedPost3DLUTTitle;
    Variant embeddedPost3DLUTSize;
    Variant embeddedPost3DLUTData;
    Variant sidecarPost3DLUTMode;
    Variant sidecarPost3DLUTName;
    Variant sidecarPost3DLUTTitle;
    Variant sidecarPost3DLUTSize;
    Variant sidecarPost3DLUTData;

    const char* sidecarFile;

    // Pointers that hold knob references so we can access them from reader op
    DD::Image::Knob* decodeResolutionKnob;
    DD::Image::Knob* colorScienceVersionKnob;
    DD::Image::Knob* colorSpaceGamutKnob;
    DD::Image::Knob* colorSpaceGammaKnob;
    DD::Image::Knob* isoKnob;
    DD::Image::Knob* exposureKnob;
    DD::Image::Knob* colorTempKnob;
    DD::Image::Knob* tintKnob;
    DD::Image::Knob* saturationKnob;
    DD::Image::Knob* contrastKnob;
    DD::Image::Knob* midpointKnob;
    DD::Image::Knob* highlightsKnob;
    DD::Image::Knob* shadowsKnob;
    DD::Image::Knob* highlightRecoveryKnob;
};
