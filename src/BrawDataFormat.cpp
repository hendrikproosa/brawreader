// Class that holds stuff related to processing format
// This is used for storing and setting processing values in decoder
#include "BrawDataFormat.h"

BrawDataFormat::BrawDataFormat()
{
    // Default processing values
    decodeQuality.uiVal = 0;
    colorScienceVersion.uiVal = 0;
    colorSpaceGamut.bstrVal = 0;
    colorSpaceGamma.bstrVal = 0;
    iso.uiVal = 2;
    exposure.fltVal = 0.0f;
    colorTemp.fltVal = 5600.0;
    tint.fltVal = 10.0;
    saturation.fltVal = 1.0f;
    contrast.fltVal = 1.0f;
    midpoint.fltVal = 0.41f;
    highlights.fltVal = 1.0f;
    shadows.fltVal = 1.0f;
    setVideoBlackLevel.uiVal = 0;
    blackLevel.fltVal = 0.0f;
    whiteLevel.fltVal = 1.0f;
    highlightRecovery.uiVal = 0;
    analogGain.fltVal = 0.0f;
    embeddedPost3DLUTMode.uiVal = 0;
    embeddedPost3DLUTName.bstrVal = 0;
    embeddedPost3DLUTTitle.bstrVal = 0;
    embeddedPost3DLUTSize.uiVal = 0;
    embeddedPost3DLUTData.parray = nullptr;
    sidecarPost3DLUTMode.uiVal = 0;
    sidecarPost3DLUTName.bstrVal = 0;
    sidecarPost3DLUTTitle.bstrVal = 0;
    sidecarPost3DLUTSize.uiVal = 0;
    sidecarPost3DLUTData.parray = nullptr;
}
